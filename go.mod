module gitlab.com/pureharvest/jasperserver-go

go 1.16

require (
	github.com/go-openapi/errors v0.20.1
	github.com/go-openapi/runtime v0.19.31
	github.com/go-openapi/strfmt v0.20.2
	github.com/go-openapi/swag v0.19.15
	github.com/go-swagger/go-swagger v0.27.0
	github.com/retrievercommunications/jasperserver-go v0.0.0-20160725145742-81b4152cceb1
)
