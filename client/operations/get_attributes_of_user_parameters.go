// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/strfmt"
)

// NewGetAttributesOfUserParams creates a new GetAttributesOfUserParams object,
// with the default timeout for this client.
//
// Default values are not hydrated, since defaults are normally applied by the API server side.
//
// To enforce default values in parameter, use SetDefaults or WithDefaults.
func NewGetAttributesOfUserParams() *GetAttributesOfUserParams {
	return &GetAttributesOfUserParams{
		timeout: cr.DefaultTimeout,
	}
}

// NewGetAttributesOfUserParamsWithTimeout creates a new GetAttributesOfUserParams object
// with the ability to set a timeout on a request.
func NewGetAttributesOfUserParamsWithTimeout(timeout time.Duration) *GetAttributesOfUserParams {
	return &GetAttributesOfUserParams{
		timeout: timeout,
	}
}

// NewGetAttributesOfUserParamsWithContext creates a new GetAttributesOfUserParams object
// with the ability to set a context for a request.
func NewGetAttributesOfUserParamsWithContext(ctx context.Context) *GetAttributesOfUserParams {
	return &GetAttributesOfUserParams{
		Context: ctx,
	}
}

// NewGetAttributesOfUserParamsWithHTTPClient creates a new GetAttributesOfUserParams object
// with the ability to set a custom HTTPClient for a request.
func NewGetAttributesOfUserParamsWithHTTPClient(client *http.Client) *GetAttributesOfUserParams {
	return &GetAttributesOfUserParams{
		HTTPClient: client,
	}
}

/* GetAttributesOfUserParams contains all the parameters to send to the API endpoint
   for the get attributes of user operation.

   Typically these are written to a http.Request.
*/
type GetAttributesOfUserParams struct {

	// Accept.
	Accept *string

	// Embedded.
	Embedded *string

	// Name.
	PathName string

	// Name.
	QueryName *string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithDefaults hydrates default values in the get attributes of user params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *GetAttributesOfUserParams) WithDefaults() *GetAttributesOfUserParams {
	o.SetDefaults()
	return o
}

// SetDefaults hydrates default values in the get attributes of user params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *GetAttributesOfUserParams) SetDefaults() {
	// no default values defined for this parameter
}

// WithTimeout adds the timeout to the get attributes of user params
func (o *GetAttributesOfUserParams) WithTimeout(timeout time.Duration) *GetAttributesOfUserParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get attributes of user params
func (o *GetAttributesOfUserParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get attributes of user params
func (o *GetAttributesOfUserParams) WithContext(ctx context.Context) *GetAttributesOfUserParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get attributes of user params
func (o *GetAttributesOfUserParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get attributes of user params
func (o *GetAttributesOfUserParams) WithHTTPClient(client *http.Client) *GetAttributesOfUserParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get attributes of user params
func (o *GetAttributesOfUserParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the get attributes of user params
func (o *GetAttributesOfUserParams) WithAccept(accept *string) *GetAttributesOfUserParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the get attributes of user params
func (o *GetAttributesOfUserParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WithEmbedded adds the embedded to the get attributes of user params
func (o *GetAttributesOfUserParams) WithEmbedded(embedded *string) *GetAttributesOfUserParams {
	o.SetEmbedded(embedded)
	return o
}

// SetEmbedded adds the embedded to the get attributes of user params
func (o *GetAttributesOfUserParams) SetEmbedded(embedded *string) {
	o.Embedded = embedded
}

// WithPathName adds the name to the get attributes of user params
func (o *GetAttributesOfUserParams) WithPathName(name string) *GetAttributesOfUserParams {
	o.SetPathName(name)
	return o
}

// SetPathName adds the name to the get attributes of user params
func (o *GetAttributesOfUserParams) SetPathName(name string) {
	o.PathName = name
}

// WithQueryName adds the name to the get attributes of user params
func (o *GetAttributesOfUserParams) WithQueryName(name *string) *GetAttributesOfUserParams {
	o.SetQueryName(name)
	return o
}

// SetQueryName adds the name to the get attributes of user params
func (o *GetAttributesOfUserParams) SetQueryName(name *string) {
	o.QueryName = name
}

// WriteToRequest writes these params to a swagger request
func (o *GetAttributesOfUserParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}
	}

	if o.Embedded != nil {

		// query param _embedded
		var qrEmbedded string

		if o.Embedded != nil {
			qrEmbedded = *o.Embedded
		}
		qEmbedded := qrEmbedded
		if qEmbedded != "" {

			if err := r.SetQueryParam("_embedded", qEmbedded); err != nil {
				return err
			}
		}
	}

	// path param name
	if err := r.SetPathParam("name", o.PathName); err != nil {
		return err
	}

	if o.QueryName != nil {

		// query param name
		var qrName string

		if o.QueryName != nil {
			qrName = *o.QueryName
		}
		qName := qrName
		if qName != "" {

			if err := r.SetQueryParam("name", qName); err != nil {
				return err
			}
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
