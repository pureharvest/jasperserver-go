// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
)

// DeleteAttributesReader is a Reader for the DeleteAttributes structure.
type DeleteAttributesReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *DeleteAttributesReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewDeleteAttributesOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	default:
		return nil, runtime.NewAPIError("response status code does not match any response statuses defined for this endpoint in the swagger spec", response, response.Code())
	}
}

// NewDeleteAttributesOK creates a DeleteAttributesOK with default headers values
func NewDeleteAttributesOK() *DeleteAttributesOK {
	return &DeleteAttributesOK{}
}

/* DeleteAttributesOK describes a response with status code 200, with default header values.

Successful Response
*/
type DeleteAttributesOK struct {
}

func (o *DeleteAttributesOK) Error() string {
	return fmt.Sprintf("[DELETE /attributes][%d] deleteAttributesOK ", 200)
}

func (o *DeleteAttributesOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
