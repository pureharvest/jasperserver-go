// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/strfmt"
)

// NewGetContentReferenceParams creates a new GetContentReferenceParams object,
// with the default timeout for this client.
//
// Default values are not hydrated, since defaults are normally applied by the API server side.
//
// To enforce default values in parameter, use SetDefaults or WithDefaults.
func NewGetContentReferenceParams() *GetContentReferenceParams {
	return &GetContentReferenceParams{
		timeout: cr.DefaultTimeout,
	}
}

// NewGetContentReferenceParamsWithTimeout creates a new GetContentReferenceParams object
// with the ability to set a timeout on a request.
func NewGetContentReferenceParamsWithTimeout(timeout time.Duration) *GetContentReferenceParams {
	return &GetContentReferenceParams{
		timeout: timeout,
	}
}

// NewGetContentReferenceParamsWithContext creates a new GetContentReferenceParams object
// with the ability to set a context for a request.
func NewGetContentReferenceParamsWithContext(ctx context.Context) *GetContentReferenceParams {
	return &GetContentReferenceParams{
		Context: ctx,
	}
}

// NewGetContentReferenceParamsWithHTTPClient creates a new GetContentReferenceParams object
// with the ability to set a custom HTTPClient for a request.
func NewGetContentReferenceParamsWithHTTPClient(client *http.Client) *GetContentReferenceParams {
	return &GetContentReferenceParams{
		HTTPClient: client,
	}
}

/* GetContentReferenceParams contains all the parameters to send to the API endpoint
   for the get content reference operation.

   Typically these are written to a http.Request.
*/
type GetContentReferenceParams struct {

	// ID.
	ID string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithDefaults hydrates default values in the get content reference params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *GetContentReferenceParams) WithDefaults() *GetContentReferenceParams {
	o.SetDefaults()
	return o
}

// SetDefaults hydrates default values in the get content reference params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *GetContentReferenceParams) SetDefaults() {
	// no default values defined for this parameter
}

// WithTimeout adds the timeout to the get content reference params
func (o *GetContentReferenceParams) WithTimeout(timeout time.Duration) *GetContentReferenceParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get content reference params
func (o *GetContentReferenceParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get content reference params
func (o *GetContentReferenceParams) WithContext(ctx context.Context) *GetContentReferenceParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get content reference params
func (o *GetContentReferenceParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get content reference params
func (o *GetContentReferenceParams) WithHTTPClient(client *http.Client) *GetContentReferenceParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get content reference params
func (o *GetContentReferenceParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithID adds the id to the get content reference params
func (o *GetContentReferenceParams) WithID(id string) *GetContentReferenceParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the get content reference params
func (o *GetContentReferenceParams) SetID(id string) {
	o.ID = id
}

// WriteToRequest writes these params to a swagger request
func (o *GetContentReferenceParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
