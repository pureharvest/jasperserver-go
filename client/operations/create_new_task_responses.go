// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
)

// CreateNewTaskReader is a Reader for the CreateNewTask structure.
type CreateNewTaskReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *CreateNewTaskReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewCreateNewTaskOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	default:
		return nil, runtime.NewAPIError("response status code does not match any response statuses defined for this endpoint in the swagger spec", response, response.Code())
	}
}

// NewCreateNewTaskOK creates a CreateNewTaskOK with default headers values
func NewCreateNewTaskOK() *CreateNewTaskOK {
	return &CreateNewTaskOK{}
}

/* CreateNewTaskOK describes a response with status code 200, with default header values.

Successful Response
*/
type CreateNewTaskOK struct {
}

func (o *CreateNewTaskOK) Error() string {
	return fmt.Sprintf("[POST /export][%d] createNewTaskOK ", 200)
}

func (o *CreateNewTaskOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
