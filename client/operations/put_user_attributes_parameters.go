// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/strfmt"
)

// NewPutUserAttributesParams creates a new PutUserAttributesParams object,
// with the default timeout for this client.
//
// Default values are not hydrated, since defaults are normally applied by the API server side.
//
// To enforce default values in parameter, use SetDefaults or WithDefaults.
func NewPutUserAttributesParams() *PutUserAttributesParams {
	return &PutUserAttributesParams{
		timeout: cr.DefaultTimeout,
	}
}

// NewPutUserAttributesParamsWithTimeout creates a new PutUserAttributesParams object
// with the ability to set a timeout on a request.
func NewPutUserAttributesParamsWithTimeout(timeout time.Duration) *PutUserAttributesParams {
	return &PutUserAttributesParams{
		timeout: timeout,
	}
}

// NewPutUserAttributesParamsWithContext creates a new PutUserAttributesParams object
// with the ability to set a context for a request.
func NewPutUserAttributesParamsWithContext(ctx context.Context) *PutUserAttributesParams {
	return &PutUserAttributesParams{
		Context: ctx,
	}
}

// NewPutUserAttributesParamsWithHTTPClient creates a new PutUserAttributesParams object
// with the ability to set a custom HTTPClient for a request.
func NewPutUserAttributesParamsWithHTTPClient(client *http.Client) *PutUserAttributesParams {
	return &PutUserAttributesParams{
		HTTPClient: client,
	}
}

/* PutUserAttributesParams contains all the parameters to send to the API endpoint
   for the put user attributes operation.

   Typically these are written to a http.Request.
*/
type PutUserAttributesParams struct {

	// Accept.
	Accept *string

	// ContentType.
	ContentType *string

	// Embedded.
	Embedded *string

	// Name.
	PathName string

	// Name.
	QueryName *string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithDefaults hydrates default values in the put user attributes params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *PutUserAttributesParams) WithDefaults() *PutUserAttributesParams {
	o.SetDefaults()
	return o
}

// SetDefaults hydrates default values in the put user attributes params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *PutUserAttributesParams) SetDefaults() {
	// no default values defined for this parameter
}

// WithTimeout adds the timeout to the put user attributes params
func (o *PutUserAttributesParams) WithTimeout(timeout time.Duration) *PutUserAttributesParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the put user attributes params
func (o *PutUserAttributesParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the put user attributes params
func (o *PutUserAttributesParams) WithContext(ctx context.Context) *PutUserAttributesParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the put user attributes params
func (o *PutUserAttributesParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the put user attributes params
func (o *PutUserAttributesParams) WithHTTPClient(client *http.Client) *PutUserAttributesParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the put user attributes params
func (o *PutUserAttributesParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the put user attributes params
func (o *PutUserAttributesParams) WithAccept(accept *string) *PutUserAttributesParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the put user attributes params
func (o *PutUserAttributesParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WithContentType adds the contentType to the put user attributes params
func (o *PutUserAttributesParams) WithContentType(contentType *string) *PutUserAttributesParams {
	o.SetContentType(contentType)
	return o
}

// SetContentType adds the contentType to the put user attributes params
func (o *PutUserAttributesParams) SetContentType(contentType *string) {
	o.ContentType = contentType
}

// WithEmbedded adds the embedded to the put user attributes params
func (o *PutUserAttributesParams) WithEmbedded(embedded *string) *PutUserAttributesParams {
	o.SetEmbedded(embedded)
	return o
}

// SetEmbedded adds the embedded to the put user attributes params
func (o *PutUserAttributesParams) SetEmbedded(embedded *string) {
	o.Embedded = embedded
}

// WithPathName adds the name to the put user attributes params
func (o *PutUserAttributesParams) WithPathName(name string) *PutUserAttributesParams {
	o.SetPathName(name)
	return o
}

// SetPathName adds the name to the put user attributes params
func (o *PutUserAttributesParams) SetPathName(name string) {
	o.PathName = name
}

// WithQueryName adds the name to the put user attributes params
func (o *PutUserAttributesParams) WithQueryName(name *string) *PutUserAttributesParams {
	o.SetQueryName(name)
	return o
}

// SetQueryName adds the name to the put user attributes params
func (o *PutUserAttributesParams) SetQueryName(name *string) {
	o.QueryName = name
}

// WriteToRequest writes these params to a swagger request
func (o *PutUserAttributesParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}
	}

	if o.ContentType != nil {

		// header param Content-Type
		if err := r.SetHeaderParam("Content-Type", *o.ContentType); err != nil {
			return err
		}
	}

	if o.Embedded != nil {

		// query param _embedded
		var qrEmbedded string

		if o.Embedded != nil {
			qrEmbedded = *o.Embedded
		}
		qEmbedded := qrEmbedded
		if qEmbedded != "" {

			if err := r.SetQueryParam("_embedded", qEmbedded); err != nil {
				return err
			}
		}
	}

	// path param name
	if err := r.SetPathParam("name", o.PathName); err != nil {
		return err
	}

	if o.QueryName != nil {

		// query param name
		var qrName string

		if o.QueryName != nil {
			qrName = *o.QueryName
		}
		qName := qrName
		if qName != "" {

			if err := r.SetQueryParam("name", qName); err != nil {
				return err
			}
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
