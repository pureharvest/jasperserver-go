// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/strfmt"
)

// NewCreatePermissionsParams creates a new CreatePermissionsParams object,
// with the default timeout for this client.
//
// Default values are not hydrated, since defaults are normally applied by the API server side.
//
// To enforce default values in parameter, use SetDefaults or WithDefaults.
func NewCreatePermissionsParams() *CreatePermissionsParams {
	return &CreatePermissionsParams{
		timeout: cr.DefaultTimeout,
	}
}

// NewCreatePermissionsParamsWithTimeout creates a new CreatePermissionsParams object
// with the ability to set a timeout on a request.
func NewCreatePermissionsParamsWithTimeout(timeout time.Duration) *CreatePermissionsParams {
	return &CreatePermissionsParams{
		timeout: timeout,
	}
}

// NewCreatePermissionsParamsWithContext creates a new CreatePermissionsParams object
// with the ability to set a context for a request.
func NewCreatePermissionsParamsWithContext(ctx context.Context) *CreatePermissionsParams {
	return &CreatePermissionsParams{
		Context: ctx,
	}
}

// NewCreatePermissionsParamsWithHTTPClient creates a new CreatePermissionsParams object
// with the ability to set a custom HTTPClient for a request.
func NewCreatePermissionsParamsWithHTTPClient(client *http.Client) *CreatePermissionsParams {
	return &CreatePermissionsParams{
		HTTPClient: client,
	}
}

/* CreatePermissionsParams contains all the parameters to send to the API endpoint
   for the create permissions operation.

   Typically these are written to a http.Request.
*/
type CreatePermissionsParams struct {
	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithDefaults hydrates default values in the create permissions params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *CreatePermissionsParams) WithDefaults() *CreatePermissionsParams {
	o.SetDefaults()
	return o
}

// SetDefaults hydrates default values in the create permissions params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *CreatePermissionsParams) SetDefaults() {
	// no default values defined for this parameter
}

// WithTimeout adds the timeout to the create permissions params
func (o *CreatePermissionsParams) WithTimeout(timeout time.Duration) *CreatePermissionsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the create permissions params
func (o *CreatePermissionsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the create permissions params
func (o *CreatePermissionsParams) WithContext(ctx context.Context) *CreatePermissionsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the create permissions params
func (o *CreatePermissionsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the create permissions params
func (o *CreatePermissionsParams) WithHTTPClient(client *http.Client) *CreatePermissionsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the create permissions params
func (o *CreatePermissionsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WriteToRequest writes these params to a swagger request
func (o *CreatePermissionsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
