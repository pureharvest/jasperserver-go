// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
)

// UpdateJobsWithProcessedParametersReader is a Reader for the UpdateJobsWithProcessedParameters structure.
type UpdateJobsWithProcessedParametersReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *UpdateJobsWithProcessedParametersReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewUpdateJobsWithProcessedParametersOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	default:
		return nil, runtime.NewAPIError("response status code does not match any response statuses defined for this endpoint in the swagger spec", response, response.Code())
	}
}

// NewUpdateJobsWithProcessedParametersOK creates a UpdateJobsWithProcessedParametersOK with default headers values
func NewUpdateJobsWithProcessedParametersOK() *UpdateJobsWithProcessedParametersOK {
	return &UpdateJobsWithProcessedParametersOK{}
}

/* UpdateJobsWithProcessedParametersOK describes a response with status code 200, with default header values.

Successful Response
*/
type UpdateJobsWithProcessedParametersOK struct {
}

func (o *UpdateJobsWithProcessedParametersOK) Error() string {
	return fmt.Sprintf("[POST /jobs][%d] updateJobsWithProcessedParametersOK ", 200)
}

func (o *UpdateJobsWithProcessedParametersOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
