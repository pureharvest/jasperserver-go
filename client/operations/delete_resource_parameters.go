// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/strfmt"
)

// NewDeleteResourceParams creates a new DeleteResourceParams object,
// with the default timeout for this client.
//
// Default values are not hydrated, since defaults are normally applied by the API server side.
//
// To enforce default values in parameter, use SetDefaults or WithDefaults.
func NewDeleteResourceParams() *DeleteResourceParams {
	return &DeleteResourceParams{
		timeout: cr.DefaultTimeout,
	}
}

// NewDeleteResourceParamsWithTimeout creates a new DeleteResourceParams object
// with the ability to set a timeout on a request.
func NewDeleteResourceParamsWithTimeout(timeout time.Duration) *DeleteResourceParams {
	return &DeleteResourceParams{
		timeout: timeout,
	}
}

// NewDeleteResourceParamsWithContext creates a new DeleteResourceParams object
// with the ability to set a context for a request.
func NewDeleteResourceParamsWithContext(ctx context.Context) *DeleteResourceParams {
	return &DeleteResourceParams{
		Context: ctx,
	}
}

// NewDeleteResourceParamsWithHTTPClient creates a new DeleteResourceParams object
// with the ability to set a custom HTTPClient for a request.
func NewDeleteResourceParamsWithHTTPClient(client *http.Client) *DeleteResourceParams {
	return &DeleteResourceParams{
		HTTPClient: client,
	}
}

/* DeleteResourceParams contains all the parameters to send to the API endpoint
   for the delete resource operation.

   Typically these are written to a http.Request.
*/
type DeleteResourceParams struct {

	// URI.
	URI string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithDefaults hydrates default values in the delete resource params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *DeleteResourceParams) WithDefaults() *DeleteResourceParams {
	o.SetDefaults()
	return o
}

// SetDefaults hydrates default values in the delete resource params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *DeleteResourceParams) SetDefaults() {
	// no default values defined for this parameter
}

// WithTimeout adds the timeout to the delete resource params
func (o *DeleteResourceParams) WithTimeout(timeout time.Duration) *DeleteResourceParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the delete resource params
func (o *DeleteResourceParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the delete resource params
func (o *DeleteResourceParams) WithContext(ctx context.Context) *DeleteResourceParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the delete resource params
func (o *DeleteResourceParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the delete resource params
func (o *DeleteResourceParams) WithHTTPClient(client *http.Client) *DeleteResourceParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the delete resource params
func (o *DeleteResourceParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithURI adds the uri to the delete resource params
func (o *DeleteResourceParams) WithURI(uri string) *DeleteResourceParams {
	o.SetURI(uri)
	return o
}

// SetURI adds the uri to the delete resource params
func (o *DeleteResourceParams) SetURI(uri string) {
	o.URI = uri
}

// WriteToRequest writes these params to a swagger request
func (o *DeleteResourceParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param uri
	if err := r.SetPathParam("uri", o.URI); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
