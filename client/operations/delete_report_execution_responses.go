// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
)

// DeleteReportExecutionReader is a Reader for the DeleteReportExecution structure.
type DeleteReportExecutionReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *DeleteReportExecutionReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewDeleteReportExecutionOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	default:
		return nil, runtime.NewAPIError("response status code does not match any response statuses defined for this endpoint in the swagger spec", response, response.Code())
	}
}

// NewDeleteReportExecutionOK creates a DeleteReportExecutionOK with default headers values
func NewDeleteReportExecutionOK() *DeleteReportExecutionOK {
	return &DeleteReportExecutionOK{}
}

/* DeleteReportExecutionOK describes a response with status code 200, with default header values.

Successful Response
*/
type DeleteReportExecutionOK struct {
}

func (o *DeleteReportExecutionOK) Error() string {
	return fmt.Sprintf("[DELETE /reportExecutions/{executionId}][%d] deleteReportExecutionOK ", 200)
}

func (o *DeleteReportExecutionOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
