// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/strfmt"
)

// NewScheduleJobsOnceNowParams creates a new ScheduleJobsOnceNowParams object,
// with the default timeout for this client.
//
// Default values are not hydrated, since defaults are normally applied by the API server side.
//
// To enforce default values in parameter, use SetDefaults or WithDefaults.
func NewScheduleJobsOnceNowParams() *ScheduleJobsOnceNowParams {
	return &ScheduleJobsOnceNowParams{
		timeout: cr.DefaultTimeout,
	}
}

// NewScheduleJobsOnceNowParamsWithTimeout creates a new ScheduleJobsOnceNowParams object
// with the ability to set a timeout on a request.
func NewScheduleJobsOnceNowParamsWithTimeout(timeout time.Duration) *ScheduleJobsOnceNowParams {
	return &ScheduleJobsOnceNowParams{
		timeout: timeout,
	}
}

// NewScheduleJobsOnceNowParamsWithContext creates a new ScheduleJobsOnceNowParams object
// with the ability to set a context for a request.
func NewScheduleJobsOnceNowParamsWithContext(ctx context.Context) *ScheduleJobsOnceNowParams {
	return &ScheduleJobsOnceNowParams{
		Context: ctx,
	}
}

// NewScheduleJobsOnceNowParamsWithHTTPClient creates a new ScheduleJobsOnceNowParams object
// with the ability to set a custom HTTPClient for a request.
func NewScheduleJobsOnceNowParamsWithHTTPClient(client *http.Client) *ScheduleJobsOnceNowParams {
	return &ScheduleJobsOnceNowParams{
		HTTPClient: client,
	}
}

/* ScheduleJobsOnceNowParams contains all the parameters to send to the API endpoint
   for the schedule jobs once now operation.

   Typically these are written to a http.Request.
*/
type ScheduleJobsOnceNowParams struct {
	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithDefaults hydrates default values in the schedule jobs once now params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *ScheduleJobsOnceNowParams) WithDefaults() *ScheduleJobsOnceNowParams {
	o.SetDefaults()
	return o
}

// SetDefaults hydrates default values in the schedule jobs once now params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *ScheduleJobsOnceNowParams) SetDefaults() {
	// no default values defined for this parameter
}

// WithTimeout adds the timeout to the schedule jobs once now params
func (o *ScheduleJobsOnceNowParams) WithTimeout(timeout time.Duration) *ScheduleJobsOnceNowParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the schedule jobs once now params
func (o *ScheduleJobsOnceNowParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the schedule jobs once now params
func (o *ScheduleJobsOnceNowParams) WithContext(ctx context.Context) *ScheduleJobsOnceNowParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the schedule jobs once now params
func (o *ScheduleJobsOnceNowParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the schedule jobs once now params
func (o *ScheduleJobsOnceNowParams) WithHTTPClient(client *http.Client) *ScheduleJobsOnceNowParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the schedule jobs once now params
func (o *ScheduleJobsOnceNowParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WriteToRequest writes these params to a swagger request
func (o *ScheduleJobsOnceNowParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
