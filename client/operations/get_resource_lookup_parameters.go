// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/strfmt"
)

// NewGetResourceLookupParams creates a new GetResourceLookupParams object,
// with the default timeout for this client.
//
// Default values are not hydrated, since defaults are normally applied by the API server side.
//
// To enforce default values in parameter, use SetDefaults or WithDefaults.
func NewGetResourceLookupParams() *GetResourceLookupParams {
	return &GetResourceLookupParams{
		timeout: cr.DefaultTimeout,
	}
}

// NewGetResourceLookupParamsWithTimeout creates a new GetResourceLookupParams object
// with the ability to set a timeout on a request.
func NewGetResourceLookupParamsWithTimeout(timeout time.Duration) *GetResourceLookupParams {
	return &GetResourceLookupParams{
		timeout: timeout,
	}
}

// NewGetResourceLookupParamsWithContext creates a new GetResourceLookupParams object
// with the ability to set a context for a request.
func NewGetResourceLookupParamsWithContext(ctx context.Context) *GetResourceLookupParams {
	return &GetResourceLookupParams{
		Context: ctx,
	}
}

// NewGetResourceLookupParamsWithHTTPClient creates a new GetResourceLookupParams object
// with the ability to set a custom HTTPClient for a request.
func NewGetResourceLookupParamsWithHTTPClient(client *http.Client) *GetResourceLookupParams {
	return &GetResourceLookupParams{
		HTTPClient: client,
	}
}

/* GetResourceLookupParams contains all the parameters to send to the API endpoint
   for the get resource lookup operation.

   Typically these are written to a http.Request.
*/
type GetResourceLookupParams struct {

	// URI.
	URI string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithDefaults hydrates default values in the get resource lookup params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *GetResourceLookupParams) WithDefaults() *GetResourceLookupParams {
	o.SetDefaults()
	return o
}

// SetDefaults hydrates default values in the get resource lookup params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *GetResourceLookupParams) SetDefaults() {
	// no default values defined for this parameter
}

// WithTimeout adds the timeout to the get resource lookup params
func (o *GetResourceLookupParams) WithTimeout(timeout time.Duration) *GetResourceLookupParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get resource lookup params
func (o *GetResourceLookupParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get resource lookup params
func (o *GetResourceLookupParams) WithContext(ctx context.Context) *GetResourceLookupParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get resource lookup params
func (o *GetResourceLookupParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get resource lookup params
func (o *GetResourceLookupParams) WithHTTPClient(client *http.Client) *GetResourceLookupParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get resource lookup params
func (o *GetResourceLookupParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithURI adds the uri to the get resource lookup params
func (o *GetResourceLookupParams) WithURI(uri string) *GetResourceLookupParams {
	o.SetURI(uri)
	return o
}

// SetURI adds the uri to the get resource lookup params
func (o *GetResourceLookupParams) SetURI(uri string) {
	o.URI = uri
}

// WriteToRequest writes these params to a swagger request
func (o *GetResourceLookupParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param uri
	if err := r.SetPathParam("uri", o.URI); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
