// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// NewGetThumbnailsParams creates a new GetThumbnailsParams object,
// with the default timeout for this client.
//
// Default values are not hydrated, since defaults are normally applied by the API server side.
//
// To enforce default values in parameter, use SetDefaults or WithDefaults.
func NewGetThumbnailsParams() *GetThumbnailsParams {
	return &GetThumbnailsParams{
		timeout: cr.DefaultTimeout,
	}
}

// NewGetThumbnailsParamsWithTimeout creates a new GetThumbnailsParams object
// with the ability to set a timeout on a request.
func NewGetThumbnailsParamsWithTimeout(timeout time.Duration) *GetThumbnailsParams {
	return &GetThumbnailsParams{
		timeout: timeout,
	}
}

// NewGetThumbnailsParamsWithContext creates a new GetThumbnailsParams object
// with the ability to set a context for a request.
func NewGetThumbnailsParamsWithContext(ctx context.Context) *GetThumbnailsParams {
	return &GetThumbnailsParams{
		Context: ctx,
	}
}

// NewGetThumbnailsParamsWithHTTPClient creates a new GetThumbnailsParams object
// with the ability to set a custom HTTPClient for a request.
func NewGetThumbnailsParamsWithHTTPClient(client *http.Client) *GetThumbnailsParams {
	return &GetThumbnailsParams{
		HTTPClient: client,
	}
}

/* GetThumbnailsParams contains all the parameters to send to the API endpoint
   for the get thumbnails operation.

   Typically these are written to a http.Request.
*/
type GetThumbnailsParams struct {

	// Accept.
	Accept *string

	// DefaultAllowed.
	DefaultAllowed *bool

	// URI.
	URI *string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithDefaults hydrates default values in the get thumbnails params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *GetThumbnailsParams) WithDefaults() *GetThumbnailsParams {
	o.SetDefaults()
	return o
}

// SetDefaults hydrates default values in the get thumbnails params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *GetThumbnailsParams) SetDefaults() {
	// no default values defined for this parameter
}

// WithTimeout adds the timeout to the get thumbnails params
func (o *GetThumbnailsParams) WithTimeout(timeout time.Duration) *GetThumbnailsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get thumbnails params
func (o *GetThumbnailsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get thumbnails params
func (o *GetThumbnailsParams) WithContext(ctx context.Context) *GetThumbnailsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get thumbnails params
func (o *GetThumbnailsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get thumbnails params
func (o *GetThumbnailsParams) WithHTTPClient(client *http.Client) *GetThumbnailsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get thumbnails params
func (o *GetThumbnailsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the get thumbnails params
func (o *GetThumbnailsParams) WithAccept(accept *string) *GetThumbnailsParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the get thumbnails params
func (o *GetThumbnailsParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WithDefaultAllowed adds the defaultAllowed to the get thumbnails params
func (o *GetThumbnailsParams) WithDefaultAllowed(defaultAllowed *bool) *GetThumbnailsParams {
	o.SetDefaultAllowed(defaultAllowed)
	return o
}

// SetDefaultAllowed adds the defaultAllowed to the get thumbnails params
func (o *GetThumbnailsParams) SetDefaultAllowed(defaultAllowed *bool) {
	o.DefaultAllowed = defaultAllowed
}

// WithURI adds the uri to the get thumbnails params
func (o *GetThumbnailsParams) WithURI(uri *string) *GetThumbnailsParams {
	o.SetURI(uri)
	return o
}

// SetURI adds the uri to the get thumbnails params
func (o *GetThumbnailsParams) SetURI(uri *string) {
	o.URI = uri
}

// WriteToRequest writes these params to a swagger request
func (o *GetThumbnailsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}
	}

	if o.DefaultAllowed != nil {

		// query param defaultAllowed
		var qrDefaultAllowed bool

		if o.DefaultAllowed != nil {
			qrDefaultAllowed = *o.DefaultAllowed
		}
		qDefaultAllowed := swag.FormatBool(qrDefaultAllowed)
		if qDefaultAllowed != "" {

			if err := r.SetQueryParam("defaultAllowed", qDefaultAllowed); err != nil {
				return err
			}
		}
	}

	if o.URI != nil {

		// query param uri
		var qrURI string

		if o.URI != nil {
			qrURI = *o.URI
		}
		qURI := qrURI
		if qURI != "" {

			if err := r.SetQueryParam("uri", qURI); err != nil {
				return err
			}
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
