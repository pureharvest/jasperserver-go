// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
)

// GetUserWorkflowReader is a Reader for the GetUserWorkflow structure.
type GetUserWorkflowReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetUserWorkflowReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewGetUserWorkflowOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	default:
		return nil, runtime.NewAPIError("response status code does not match any response statuses defined for this endpoint in the swagger spec", response, response.Code())
	}
}

// NewGetUserWorkflowOK creates a GetUserWorkflowOK with default headers values
func NewGetUserWorkflowOK() *GetUserWorkflowOK {
	return &GetUserWorkflowOK{}
}

/* GetUserWorkflowOK describes a response with status code 200, with default header values.

Successful Response
*/
type GetUserWorkflowOK struct {
}

func (o *GetUserWorkflowOK) Error() string {
	return fmt.Sprintf("[GET /hypermedia/workflows/{name}][%d] getUserWorkflowOK ", 200)
}

func (o *GetUserWorkflowOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
