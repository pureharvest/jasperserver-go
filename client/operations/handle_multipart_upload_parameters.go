// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/strfmt"
)

// NewHandleMultipartUploadParams creates a new HandleMultipartUploadParams object,
// with the default timeout for this client.
//
// Default values are not hydrated, since defaults are normally applied by the API server side.
//
// To enforce default values in parameter, use SetDefaults or WithDefaults.
func NewHandleMultipartUploadParams() *HandleMultipartUploadParams {
	return &HandleMultipartUploadParams{
		timeout: cr.DefaultTimeout,
	}
}

// NewHandleMultipartUploadParamsWithTimeout creates a new HandleMultipartUploadParams object
// with the ability to set a timeout on a request.
func NewHandleMultipartUploadParamsWithTimeout(timeout time.Duration) *HandleMultipartUploadParams {
	return &HandleMultipartUploadParams{
		timeout: timeout,
	}
}

// NewHandleMultipartUploadParamsWithContext creates a new HandleMultipartUploadParams object
// with the ability to set a context for a request.
func NewHandleMultipartUploadParamsWithContext(ctx context.Context) *HandleMultipartUploadParams {
	return &HandleMultipartUploadParams{
		Context: ctx,
	}
}

// NewHandleMultipartUploadParamsWithHTTPClient creates a new HandleMultipartUploadParams object
// with the ability to set a custom HTTPClient for a request.
func NewHandleMultipartUploadParamsWithHTTPClient(client *http.Client) *HandleMultipartUploadParams {
	return &HandleMultipartUploadParams{
		HTTPClient: client,
	}
}

/* HandleMultipartUploadParams contains all the parameters to send to the API endpoint
   for the handle multipart upload operation.

   Typically these are written to a http.Request.
*/
type HandleMultipartUploadParams struct {
	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithDefaults hydrates default values in the handle multipart upload params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *HandleMultipartUploadParams) WithDefaults() *HandleMultipartUploadParams {
	o.SetDefaults()
	return o
}

// SetDefaults hydrates default values in the handle multipart upload params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *HandleMultipartUploadParams) SetDefaults() {
	// no default values defined for this parameter
}

// WithTimeout adds the timeout to the handle multipart upload params
func (o *HandleMultipartUploadParams) WithTimeout(timeout time.Duration) *HandleMultipartUploadParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the handle multipart upload params
func (o *HandleMultipartUploadParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the handle multipart upload params
func (o *HandleMultipartUploadParams) WithContext(ctx context.Context) *HandleMultipartUploadParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the handle multipart upload params
func (o *HandleMultipartUploadParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the handle multipart upload params
func (o *HandleMultipartUploadParams) WithHTTPClient(client *http.Client) *HandleMultipartUploadParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the handle multipart upload params
func (o *HandleMultipartUploadParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WriteToRequest writes these params to a swagger request
func (o *HandleMultipartUploadParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
