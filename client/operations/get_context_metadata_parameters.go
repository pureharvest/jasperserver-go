// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/strfmt"
)

// NewGetContextMetadataParams creates a new GetContextMetadataParams object,
// with the default timeout for this client.
//
// Default values are not hydrated, since defaults are normally applied by the API server side.
//
// To enforce default values in parameter, use SetDefaults or WithDefaults.
func NewGetContextMetadataParams() *GetContextMetadataParams {
	return &GetContextMetadataParams{
		timeout: cr.DefaultTimeout,
	}
}

// NewGetContextMetadataParamsWithTimeout creates a new GetContextMetadataParams object
// with the ability to set a timeout on a request.
func NewGetContextMetadataParamsWithTimeout(timeout time.Duration) *GetContextMetadataParams {
	return &GetContextMetadataParams{
		timeout: timeout,
	}
}

// NewGetContextMetadataParamsWithContext creates a new GetContextMetadataParams object
// with the ability to set a context for a request.
func NewGetContextMetadataParamsWithContext(ctx context.Context) *GetContextMetadataParams {
	return &GetContextMetadataParams{
		Context: ctx,
	}
}

// NewGetContextMetadataParamsWithHTTPClient creates a new GetContextMetadataParams object
// with the ability to set a custom HTTPClient for a request.
func NewGetContextMetadataParamsWithHTTPClient(client *http.Client) *GetContextMetadataParams {
	return &GetContextMetadataParams{
		HTTPClient: client,
	}
}

/* GetContextMetadataParams contains all the parameters to send to the API endpoint
   for the get context metadata operation.

   Typically these are written to a http.Request.
*/
type GetContextMetadataParams struct {

	// Path.
	Path string

	// UUID.
	UUID string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithDefaults hydrates default values in the get context metadata params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *GetContextMetadataParams) WithDefaults() *GetContextMetadataParams {
	o.SetDefaults()
	return o
}

// SetDefaults hydrates default values in the get context metadata params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *GetContextMetadataParams) SetDefaults() {
	// no default values defined for this parameter
}

// WithTimeout adds the timeout to the get context metadata params
func (o *GetContextMetadataParams) WithTimeout(timeout time.Duration) *GetContextMetadataParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get context metadata params
func (o *GetContextMetadataParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get context metadata params
func (o *GetContextMetadataParams) WithContext(ctx context.Context) *GetContextMetadataParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get context metadata params
func (o *GetContextMetadataParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get context metadata params
func (o *GetContextMetadataParams) WithHTTPClient(client *http.Client) *GetContextMetadataParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get context metadata params
func (o *GetContextMetadataParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithPath adds the path to the get context metadata params
func (o *GetContextMetadataParams) WithPath(path string) *GetContextMetadataParams {
	o.SetPath(path)
	return o
}

// SetPath adds the path to the get context metadata params
func (o *GetContextMetadataParams) SetPath(path string) {
	o.Path = path
}

// WithUUID adds the uuid to the get context metadata params
func (o *GetContextMetadataParams) WithUUID(uuid string) *GetContextMetadataParams {
	o.SetUUID(uuid)
	return o
}

// SetUUID adds the uuid to the get context metadata params
func (o *GetContextMetadataParams) SetUUID(uuid string) {
	o.UUID = uuid
}

// WriteToRequest writes these params to a swagger request
func (o *GetContextMetadataParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param path
	if err := r.SetPathParam("path", o.Path); err != nil {
		return err
	}

	// path param uuid
	if err := r.SetPathParam("uuid", o.UUID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
